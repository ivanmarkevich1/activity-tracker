FROM node:14

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install -g typescript

RUN npm install

COPY . .

RUN tsc

EXPOSE $PORT
CMD [ "node", "./dist/server.js"]