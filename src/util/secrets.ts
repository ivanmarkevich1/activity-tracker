import dotenv from "dotenv";

dotenv.config();

export const JWT_SECRET = process.env["JWT_SECRET"];
export const MONGODB_URI = process.env["MONGODB_URI"];
export const REDIS_URI = process.env["REDIS_URI"];

if (!MONGODB_URI) {
  console.error("No mongo uri provided. Exiting...");
  process.exit(1);
}

if (!REDIS_URI) {
  console.error("No redis uri provided. Exiting...");
  process.exit(1);
}

if (!JWT_SECRET) {
  console.error("No secret provided. Set JWT_SECRET environment variable.");
  process.exit(1);
}
