import { Moment } from "moment";

export class DatePeriod {
  private readonly startDate: Moment;
  private readonly endDate: Moment;

  constructor(startDate: Moment, endDate: Moment) {
    if (startDate.isAfter(endDate)) {
      throw new Error(
        `Incorrect date period. ${startDate.toDate()} is after ${endDate.toDate()}.`
      );
    }

    this.startDate = startDate;
    this.endDate = endDate;
  }

  public getStartDate(): Moment {
    return this.startDate;
  }

  public getEndDate(): Moment {
    return this.endDate;
  }
}

export class Dates {
  public static getOverlap(
    firstPeriod: DatePeriod,
    secondPeriod: DatePeriod
  ): DatePeriod | null {
    if (!firstPeriod.getStartDate().isSameOrBefore(secondPeriod.getEndDate())) {
      return null;
    }

    const overlapStartDate: Moment = firstPeriod
      .getStartDate()
      .isBefore(secondPeriod.getStartDate())
      ? secondPeriod.getStartDate()
      : firstPeriod.getStartDate();
    const overlapEndDate: Moment = firstPeriod
      .getEndDate()
      .isBefore(secondPeriod.getEndDate())
      ? firstPeriod.getEndDate()
      : secondPeriod.getEndDate();
    return new DatePeriod(overlapStartDate, overlapEndDate);
  }
}
