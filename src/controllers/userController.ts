import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import passport from "passport";
import "../config/passport";
import { User } from "../models/User";
import { JWT_SECRET } from "../util/secrets";

export class UserController {
  public async registerUser(req: Request, res: Response): Promise<void> {
    try {
      await User.create({
        username: req.body.username.toLowerCase(),
        password: req.body.password,
      });
    } catch (e) {
      const err = e as Error;
      res.status(500).send({ msg: err.message });
      return;
    }

    const token = jwt.sign({ username: req.body.username }, JWT_SECRET || "");
    res.status(200).send({ token: token });
  }

  public authenticateUser(req: Request, res: Response, next: NextFunction) {
    passport.authenticate("local", function (err, user, info) {
      if (err) return next(err);
      if (!user) {
        return res.status(401).json({ status: "error", code: "unauthorized" });
      }

      const token = jwt.sign({ username: user.username }, JWT_SECRET || "");
      return res.status(200).send({ token: token });
    })(req, res, next);
  }
}
