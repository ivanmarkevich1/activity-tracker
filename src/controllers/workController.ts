import { NextFunction, Request, Response } from "express";
import { UserDocument } from "../models/User";
import moment, { Moment } from "moment";
import { WorkDocument } from "../models/Work";
import { DatePeriod, Dates } from "../util/dates";
import { redisClient } from "../app";
import util from "util";

export class WorkController {
  private dateFormat: string = "YYYY-MM-DD";

  public startWork = async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    this.checkRequest(req);

    const user = req.user;
    const date = new Date();

    if (
      user.works.every(
        (workDoc) =>
          workDoc.endDate ||
          !moment(workDoc.startDate).isSame(moment(date), "date")
      ) ||
      user.works.length === 0
    ) {
      user.works.push({ startDate: date } as WorkDocument);
      user.save((err) => {
        if (err) {
          console.error(err);
          res.status(500).json({ msg: "Database error" });
          return;
        }

        console.log(user);
      });

      res.status(200).send();
      return;
    }

    res.status(500).json({ msg: "The work has already started" });
  };

  public endWork = async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    this.checkRequest(req);

    const user = req.user;
    const lastWorkIndex = user.works.length - 1;

    if (user.works[lastWorkIndex].endDate === undefined) {
      user.works[lastWorkIndex].endDate = new Date();
      user.save((err) => {
        if (err) {
          res.status(500).json({ msg: err.message });
          return;
        }
      });

      res.status(200).send();
      return;
    }

    res.status(500).json({ msg: "The work has already finished" });
    return;
  };

  public workTime = async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    this.checkRequest(req);

    const user = req.user;
    const fromDate: string | undefined = req.query.fromDate as
      | string
      | undefined;
    const toDate: string | undefined = req.query.toDate as string | undefined;
    const redisKey = user.username + fromDate + toDate;

    const redisGetAsync = util.promisify(redisClient.get).bind(redisClient);
    const hours = await redisGetAsync(redisKey);
    if (hours) {
      res.status(200).send({ hours: hours, msg: "Hello from Redis" });
      return;
    }

    try {
      let queryDate: DatePeriod;
      let secs = 0;

      if (fromDate === undefined || toDate === undefined) {
        res.status(400).json({ msg: "Missing parameters" });
        return;
      }

      queryDate = new DatePeriod(
        moment(fromDate, this.dateFormat),
        moment(toDate, this.dateFormat)
      );

      user.works.forEach((workDoc) => {
        const startDate: Moment = moment(workDoc.startDate);
        const endDate: Moment = workDoc.endDate
          ? moment(workDoc.endDate)
          : moment(workDoc.startDate).set({ h: 23, m: 59 });
        let diff: number;

        const docDatePeriod = new DatePeriod(startDate, endDate);
        const overlap = Dates.getOverlap(docDatePeriod, queryDate);
        if (overlap === null) {
          return;
        }

        diff = overlap.getEndDate().diff(overlap.getStartDate(), "seconds");
        secs += diff;
      });

      const hours = moment.duration(secs, "seconds").asHours();
      redisClient.setex(redisKey, 60, hours.toString());
      res.status(200).json({ hours: hours });
    } catch (e) {
      const err = e as Error;
      res.status(500).json({ msg: err.message });
      return;
    }
  };

  private checkRequest(
    req: Request
  ): asserts req is Request & { user: UserDocument } {
    if (!("user" in req)) {
      throw new Error("Request object does not contain user");
    }
  }
}
