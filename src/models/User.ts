import bcrypt from "bcrypt";
import * as mongoose from "mongoose";
import { WorkDocument, workSchema } from "./Work";

export type UserDocument = mongoose.Document & {
  username: string;
  password: string;
  works: WorkDocument[];
};

type ComparePasswordFunction = (
  candidatePassword: string,
  cb: (err: any, isMatch: any) => void
) => void;

const userSchema = new mongoose.Schema<UserDocument>({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },

  works: [workSchema],
});

//hash the password
userSchema.pre("save", async function save(next) {
  const user = this as UserDocument;
  if (!user.isModified("password")) {
    return next();
  }

  try {
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    next();
  } catch (e: any) {
    next(e);
  }
});

const comparePassword: ComparePasswordFunction = async function (
  this: any,
  candidatePassword
) {
  let isMatch;
  try {
    isMatch = await bcrypt.compare(candidatePassword, this.password);
  } catch (e) {
    return { error: e, isMatch: isMatch };
  }

  return { isMatch: isMatch };
};

userSchema.methods.comparePassword = comparePassword;

export const User = mongoose.model<UserDocument>("User", userSchema);
