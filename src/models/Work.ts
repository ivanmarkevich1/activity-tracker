import * as mongoose from "mongoose";

export type WorkDocument = mongoose.Document & {
  startDate: Date;
  endDate: Date;
};

export const workSchema = new mongoose.Schema<WorkDocument>({
  startDate: Date,
  endDate: Date,
});
