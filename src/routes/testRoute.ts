import { Router } from "express";
import { AuthController } from "../controllers/authController";

export class TestRoutes {
  router: Router;
  public authController: AuthController = new AuthController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes = () => {
    this.router.post("/jwt", this.authController.authenticateJWT);
  };
}
