import { Router } from "express";
import { WorkController } from "../controllers/workController";
import { AuthController } from "../controllers/authController";

export class WorkRoutes {
  router: Router;
  public workController: WorkController = new WorkController();
  public authController: AuthController = new AuthController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes = () => {
    this.router.post(
      "/start-work",
      this.authController.authenticateJWT,
      this.workController.startWork
    );
    this.router.post(
      "/end-work",
      this.authController.authenticateJWT,
      this.workController.endWork
    );
    this.router.get(
      "/work-time",
      this.authController.authenticateJWT,
      this.workController.workTime
    );
  };
}
