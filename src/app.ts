import express from "express";
import { MONGODB_URI, REDIS_URI } from "./util/secrets";
import mongoose, { ConnectOptions } from "mongoose";
import redis from "redis";
import passport from "passport";
import bodyParser from "body-parser";
import { UserRoutes } from "./routes/userRoutes";
import { TestRoutes } from "./routes/testRoute";
import { WorkRoutes } from "./routes/workRoutes";

// Create Express Server
const app = express();

// Connect to Mongo
const mongoUri: string = MONGODB_URI || "";
mongoose
  .connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  } as ConnectOptions)
  .then(() => {})
  .catch((err) => {
    console.error(`MongoDB connection error: ${err}`);
    process.exit();
  });

// Connect to Redis
const redisUri = REDIS_URI || "";
export const redisClient = redis.createClient(redisUri);
redisClient.on("error", (err) => {
  console.error(err);
});

// Config
app.set("port", process.env.PORT || 3000);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());

// Routes
app.use("/", new UserRoutes().router);
app.use("/test", new TestRoutes().router);
app.use("/", new WorkRoutes().router);

export default app;
